export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyB3UMcZqe1EoKNk0SHAvSfvcey-I4RnEmU",
    authDomain: "theratest-39def.firebaseapp.com",
    databaseURL: "https://theratest-39def.firebaseio.com",
    projectId: "theratest-39def",
    storageBucket: "theratest-39def.appspot.com",
    messagingSenderId: "689037042679",
    appId: "1:689037042679:web:05fc7aeb75a835006f84f2",
    measurementId: "G-478X33G0F1"
  }
};
