// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB3UMcZqe1EoKNk0SHAvSfvcey-I4RnEmU",
    authDomain: "theratest-39def.firebaseapp.com",
    databaseURL: "https://theratest-39def.firebaseio.com",
    projectId: "theratest-39def",
    storageBucket: "theratest-39def.appspot.com",
    messagingSenderId: "689037042679",
    appId: "1:689037042679:web:05fc7aeb75a835006f84f2",
    measurementId: "G-478X33G0F1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
