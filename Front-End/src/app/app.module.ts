import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
// MDB
import { MDBBootstrapModule } from 'angular-bootstrap-md';
// Ag-Grid
import { AgGridModule } from 'ag-grid-angular';

// Paginas
import { HomeComponent } from './pages/home/home.component';


// Componentes
import { HeaderComponent } from './components/header/header.component';
import { FilterComponent } from './components/filter/filter.component';
import { GridComponent } from './components/grid/grid.component';
import { FooterComponent } from './components/footer/footer.component';


// AngularFire
import { environment } from './../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';


import { CentralRxJsService } from './services/centralRXJS.service';

const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule
]


const components = [
  HeaderComponent,
  FilterComponent,
  GridComponent,
  FooterComponent
]
const formModules = [
  FormsModule,
  ReactiveFormsModule,
]
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ...components

  ],
  imports: [
    BrowserModule,
    // CDK
    BrowserAnimationsModule,
    LayoutModule,
    ...formModules,
    ...AngularFire,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    AgGridModule.withComponents([]),
    HttpClientModule,
  ],
  providers: [
    CentralRxJsService
  ],
  bootstrap: [AppComponent],
  entryComponents: [HeaderComponent, FilterComponent, GridComponent, FooterComponent]
})
export class AppModule { }
