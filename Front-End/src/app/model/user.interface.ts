export interface UserInterface {
  usuario: string;
  email: string;
  dataInclusao: string;
  dataAlteracao: string;
  regras: number;
  status: boolean;
}
