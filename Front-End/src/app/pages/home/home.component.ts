import { Component, OnInit } from '@angular/core';
import { PublishDataService } from 'src/app/services/publishData.service';
import { GridOptions, GridApi, ColDef, ICellRendererParams } from 'ag-grid-community';
import { UserInterface } from 'src/app/model/user.interface';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { SenderRXJS } from 'src/app/model/senderRXJS.interface';
import { config } from 'src/app/services/config';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  userList: UserInterface[] = [];
  pageGridSize = null;
  GridPage = {
    current: null,
    total: null
  };

  constructor(
    private datasrv: PublishDataService,
    private centralRXJS: CentralRxJsService
  ) {
   // this.startGrid();
  }

  ngOnInit() {
    this.datasrv.userList.subscribe((resUser) => {
      const comando: SenderRXJS = {
        key: config.rxjsCentralKeys.lastUpdate,
        data: new String(new Date().getHours() + ':' + new Date().getMinutes())
      };
      this.centralRXJS.sendData = comando;
      this.userList = resUser;

    });
  }

/*   goToGridPage(whereTo: string) {
    console.log(whereTo);
    console.log(this.gridApi.paginationGetCurrentPage());
    console.log(this.gridApi.paginationGetTotalPages());
    switch (whereTo) {
      case 'first':
        this.gridApi.paginationGoToFirstPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;
      case 'last':
        this.gridApi.paginationGoToLastPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;
      case 'next':
        this.gridApi.paginationGoToNextPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;
      case 'previous':
        this.gridApi.paginationGoToPreviousPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;


    }

  } */
}
