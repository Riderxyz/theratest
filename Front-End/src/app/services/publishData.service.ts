import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { GridOptions, GridApi } from 'ag-grid-community';
import { UserInterface } from '../model/user.interface';
declare var Chance: any;
@Injectable({ providedIn: 'root' })
export class PublishDataService {
  chance: any;
  arrUser = [];
  public gridApi: GridApi;
  public gridOptions: GridOptions;
  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase) {
    this.chance = new Chance();
    console.log(this.chance.ip());
  }


  publishFakeData() {
    //
    for (let i = 0; i < 500; i++) {
/*       this.arrUser.push({
        usuario: this.chance.first(),
        email: this.chance.email({ domain: 'tvglobo.com.br' }),
        dataInclusao: this.chance.date({ string: true }),
        dataAlteracao: this.chance.date({ string: true }),
        regras: this.chance.integer({ min: 0, max: 10 }),
        status: this.chance.bool()
      }) */
      this.db.list('/usuarios').push({
        usuario: this.chance.first(),
        email: this.chance.email({ domain: 'tvglobo.com.br' }),
        dataInclusao: this.chance.date({ string: true }),
        dataAlteracao: this.chance.date({ string: true }),
        regras: this.chance.integer({ min: 0, max: 10 }),
        status: this.chance.bool()
      })
    }
 //   console.log(JSON.stringify(this.arrUser));

  }

  get userList() {
    console.log(123456789);

    return this.db.list<UserInterface>('usuarios').valueChanges();
  }
}
