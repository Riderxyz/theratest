import { Component, OnInit } from '@angular/core';
import { PublishDataService } from './services/publishData.service';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { CentralRxJsService } from './services/centralRXJS.service';
import { config } from './services/config';
import { SenderRXJS } from './model/senderRXJS.interface';
declare var Chance: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'TheraDashBoard';
  constructor(
    private datasrv: PublishDataService,
    public breakpointObserver: BreakpointObserver,
    private centralRXJS: CentralRxJsService
  ) {
    // this.datasrv.publishFakeData();
  }

  ngOnInit(): void {
    let comando: SenderRXJS = {
      key: null
    }

    this.breakpointObserver
      .observe(['(min-width: 1000px)'])
      .subscribe((state: BreakpointState) => {
        console.log('Fui ativado');
        if (state.matches) {
          console.log('Viewport is 500px or over!', state);
          comando.key = config.rxjsCentralKeys.ChangeToWeb;
          this.centralRXJS.sendData = comando;
          /* this.ShowWhenSizable = false; */
        } else {
          console.log('Viewport is getting smaller!', state);
          comando.key = config.rxjsCentralKeys.ChangeToMobile;
          this.centralRXJS.sendData = comando;
          /* this.ShowWhenSizable = true; */
        }
      });
  }
}

