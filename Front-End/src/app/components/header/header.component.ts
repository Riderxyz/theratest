import { Component, OnInit } from '@angular/core';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { config } from '../../services/config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
isMobile = false;
filtro = '';
  constructor(
    private centralRXJS: CentralRxJsService
  ) { }

  ngOnInit() {
    this.centralRXJS.DataToReceive.subscribe((res) => {
      if (res.key === config.rxjsCentralKeys.ChangeToMobile) {
        this.isMobile = true;
      }
      if (res.key === config.rxjsCentralKeys.ChangeToWeb) {
        this.isMobile = false;
      }
    })
  }

  openSideMenu() {
    this.centralRXJS.sendData = {
      key: config.rxjsCentralKeys.openSideMenu
    };
  }
  startFilter(ev) {
    console.log(ev);
    this.centralRXJS.sendData = {
      key: config.rxjsCentralKeys.onGridFilter,
      data: ev
    };
  }

}
