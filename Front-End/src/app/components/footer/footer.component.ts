import { Component, OnInit } from '@angular/core';
import { PublishDataService } from 'src/app/services/publishData.service';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { SenderRXJS } from 'src/app/model/senderRXJS.interface';
import { config } from 'src/app/services/config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  lastUpdate: any = null;
  constructor(
    public dataSrv: PublishDataService,
    private centralRXJS: CentralRxJsService
  ) {


  }

  ngOnInit() {
    this.centralRXJS.DataToReceive
      .subscribe((res: SenderRXJS) => {
        if (res.key === config.rxjsCentralKeys.lastUpdate) {
          this.lastUpdate = res.data;
        }
      })
  }

}
