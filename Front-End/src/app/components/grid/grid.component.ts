import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { PublishDataService } from 'src/app/services/publishData.service';
import { GridOptions, GridApi, ColumnApi, ColDef, ICellRendererParams } from 'ag-grid-community';
import { UserInterface } from 'src/app/model/user.interface';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { SenderRXJS } from 'src/app/model/senderRXJS.interface';
import { config } from '../../services/config';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
  public gridApi: GridApi;
  public gridOptions: GridOptions;
  public gridColumnApi: ColumnApi;
  grid = {
    Api: null as GridApi,
    Options: null as GridOptions,
    ColumnApi: null as ColumnApi,
  };
  columnDefs: ColDef[] = [];
  @Input() hasControl: boolean;
  @Input() itemsPerPage: number;
  @Input() data: UserInterface[];
  filtro = '';
  isGridReady = false;
  GridPage = {
    current: null,
    total: null
  };
  constructor(
    private datasrv: PublishDataService,
    private centralRXJS: CentralRxJsService
  ) { }

  ngOnInit() {
    this.startGrid();
    window.onresize = ((resizeObj) => {
      this.gridApi.sizeColumnsToFit();
    });
    this.centralRXJS.DataToReceive.subscribe((res) => {
      switch (res.key) {
        case config.rxjsCentralKeys.ChangeToWeb:

          break;
        case config.rxjsCentralKeys.onGridFilter:
          console.log('recebido');

          this.filtro = res.data;
          this.gridApi.onFilterChanged();
          break;

      }

    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('tive mudanças', changes.data.previousValue);
    if (changes.data.previousValue !== undefined) {
      this.gridOptions.api.setRowData(this.data);
      this.gridOptions.api.sizeColumnsToFit();
    }
  }

  startGrid() {
    this.gridOptions = {
      columnDefs: this.createColumnDefs,
      defaultColDef: {
        editable: false,
/*         enableRowGroup: true,
        enablePivot: true,
        enableValue: true, */
        sortable: true,
        resizable: true,
        filter: true,
      },
      suppressHorizontalScroll: false,
      overlayNoRowsTemplate: `
      <span class="ag-overlay-loading-center">Sem dados para exibir</span>
      `,
      overlayLoadingTemplate: `<span">carregando dados</span>`,
      suppressPaginationPanel: true,
      pagination: true,
      rowSelection: 'multiple',
      onSelectionChanged: ((params) => {
        this.onCheckBoxSelection(params);
      }),
      onRowDoubleClicked: ((params) => {
        this.onRowSelect(params);
      }),
      rowHeight: 100,
      /* angularCompileRows: true, */
      isExternalFilterPresent: this.externalFilterPresent.bind(this),
      doesExternalFilterPass: this.externalFilterPass.bind(this),
      paginationPageSize: this.itemsPerPage,
      onGridReady: (params) => {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        this.GridPage.total = this.gridApi.paginationGetTotalPages();
        // this.gridApi.paginationSetPageSize(Number(50));
      }
    };
  }


  onRowSelect(ev) {
    console.log(ev);
  }

  onCheckBoxSelection(params) {
    console.log(params);

    console.log('selecionando items da linha 95', params);
    const selectedRows = this.gridApi.getSelectedRows();
    console.log('dados da linha', selectedRows);


  }

  get createColumnDefs(): Array<ColDef> {
    const that = this;
    return this.columnDefs = [
      {
        headerName: 'USUARIO',
        field: 'usuario',
        checkboxSelection: ((params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        }),
        headerCheckboxSelection: ((params) => {
          return params.columnApi.getRowGroupColumns().length === 0;
        })
      },
      {
        headerName: 'EMAIL',
        field: 'email',

      },
      {
        headerName: 'DATA DE INCLUSÃO',
        field: 'dataInclusao',
        autoHeight: true,
      },
      {
        headerName: 'DATA DE ALTERAÇÃO',
        field: 'dataAlteracao',
        autoHeight: true,
      },
      {
        headerName: 'REGRAS',
        field: 'regras',
        autoHeight: true,
      },
      {
        headerName: 'STATUS',
        field: 'status',
        autoHeight: true,
        cellRenderer: ((params: ICellRendererParams) => {
          /* params. */
          const dados: UserInterface = params.data;
          let retorno = ``;
          if (dados.status) {
            retorno = `
             <span style="color:#31ba1f;">
                ATIVO
              </span> `;
          } else {
            retorno = `
            <span style="color:#e04551;">
               DESATIVADO
             </span> `;
          }
          return retorno;
        })
      },
    ];
  }
  externalFilterPresent() {
    return this.filtro !== '';
  }

  externalFilterPass(node: any) {
    if (this.filtro !== '') {
      if (
        ((node.data.usuario + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1)
        ||
        ((node.data.email + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1)
        ||
        ((node.data.regras + '').toLowerCase().indexOf(this.filtro.toLowerCase()) !== -1)
      ) {
        return true;
      }
    } else {
      return false;
    }
    return false;
  }

  goToGridPage(whereTo: string) {

    switch (whereTo) {
      case 'first':
        this.gridApi.paginationGoToFirstPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;
      case 'last':
        this.gridApi.paginationGoToLastPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;
      case 'next':
        this.gridApi.paginationGoToNextPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;
      case 'previous':
        this.gridApi.paginationGoToPreviousPage();
        this.GridPage.current = this.gridApi.paginationGetCurrentPage();
        break;


    }

  }
}
