import { Component, OnInit, Input, Output, ViewChild } from '@angular/core';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { config } from '../../services/config';
import { ModalDirective } from 'angular-bootstrap-md'
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Input() title: string;
  @ViewChild('sideMenu', { static: true }) sideMenu: ModalDirective;
  constructor(
    private centralRXJS: CentralRxJsService
  ) { }

  ngOnInit() {
    this.centralRXJS.DataToReceive.subscribe((res) => {
      if (res.key === config.rxjsCentralKeys.openSideMenu) {
        this.sideMenu.show();
      }
    })
  }

}
