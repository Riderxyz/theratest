import { FirstExercice } from './firstExercice';
import { ThirdExercice } from './thirdExercice';
import { SecondExercice } from './secondExercice';

class App {
    /** Entry point of our app */
    public static start() {
        const arrFirst: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const stringSecond = '{[()]}'
        const arrThird: number[] = [7, 67, 32, 56, 1, 5, 3, 6, 4];
        
        const firstExerciceRes: string = FirstExercice(arrFirst, 9)
        const secondExerciceRes = SecondExercice(stringSecond)
        const thirdExerciceRes: any = ThirdExercice(arrThird, 7)



        console.log(firstExerciceRes);
        console.log(secondExerciceRes);
        console.log(thirdExerciceRes);


    }
}
App.start();

